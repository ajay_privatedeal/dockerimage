README.md
#Prerequisite
- Docker & docker compose must be installed on the system
Install Docker- 
Windows-
Docker: https://docs.docker.com/docker-for-windows/install/
Docker Compose- https://docs.docker.com/compose/install/

Linux-
Docker: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04
Docker Compose- https://linuxize.com/post/how-to-install-and-use-docker-compose-on-ubuntu-20-04/


- Git must be installed on the syste.
- Please craete a localhost entry with system/server local IP-
127.0.0.1 privatedeal.local


# Setup new project

# step 1
Create project directory & clone this repo to the directory-

`git clone https://privatedealdevops@bitbucket.org/ajay_privatedeal/dockerimage.git

# step 2 build and run docker containers

```bash
docker-compose up -d --build
```
waiting for it to finish it takes when running it first time

# step 3
docker exec -it web sh


Copy the existing project files/folders to the `./privatedeal` folder of your project directory.

Copy existing database dump (`example privatedeal.sql`) to the `./mysql` folder of your project directory.

# step 4
Change the env file as below -
update the db host to "db, user name to "root" & password to "root" in the .env file

Restore the existing DB dump to the docker container using below command

`docker exec -i db bash -l -c "mysql -uroot -proot privatedeal < /var/lib/mysql/privatedeal.sql"`

# step 5 - stop and start containers
```bash
docker-compose down
docker-compose up -d
```

**Note:** to run **composer** follow commands as below-

`docke exec -t web /bin/bash`  then `cd /app && composer install`

Done! Magento can be access on URL `https://privatedeal.local:10443`


